﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRR1
{
    public interface IStack<T>
    {
        public void Push(T value);
        public T Pop();
        public void Clear();
    }
}
