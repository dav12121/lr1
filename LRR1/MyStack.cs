﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LRR1
{
    public class MyStack<T> : Stack<T>
    {
        private T[] _elements;
        public int Count { get; private set; }
        public T Head;
        public MyStack()
        {
            _elements = new T[10];
            Count = 0;
            Head = _elements[0];
        }

        public MyStack(T[] inputArray)
        {
            _elements = inputArray;
            Head = _elements[_elements.Length - 1];
            Count = _elements.Length;
        }

        public void Push(T value)
        {
            if (Count == 0)
            {
                _elements[Count] = value;
                Head = value;
                Count++;
            }
            else if (Count < _elements.Length)
            {
                _elements[Count] = value;
                Head = value;
                Count++;
            }
            else if (Count >= _elements.Length)
            {
                T[] tmp = new T[Count + 10];
                for (int i = 0; i < _elements.Length; i++)
                {
                    tmp[i] = _elements[i];
                }
                tmp[Count] = value;
                Head = value;
                Count++;
                _elements = tmp;
            }
        }

        public T Pop()
        {
            if (Count == 0) throw new IndexOutOfRangeException("Can not delete from empty stack");
            T value = _elements[Count - 1];
            if (Count == 1) //Удаление последнего элемента
            {
                _elements = new T[10];
                Head = _elements[0];
                Count--;
                return value;
            }
            T[] tmp = new T[Count - 1];
            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = _elements[i];
            }
            _elements = tmp;
            Head = _elements[_elements.Length - 1];
            Count--;
            return value;
        }

        public void Clear()
        {
            _elements = new T[10];
            Count = 0;
            Head = _elements[9];
        }
    }
}
