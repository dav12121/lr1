﻿using System;
using LRR1;

namespace LRR1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var stack = new MyStack<int>();
            char[] aCharArray = new char[10];
            stack.Push(5);
            stack.Push(6);
            stack.Push(7);
            stack.Push(8);
            stack.Push(9);
            stack.Push(10);
            var a = stack.Pop();
            Console.WriteLine(a);
            var b = stack.Pop();
            Console.WriteLine(b);
            var c = stack.Pop();
            Console.WriteLine(c);
            var d = stack.Pop();
            Console.WriteLine(d);
            var e = stack.Pop();
            Console.WriteLine(e);
            var f = stack.Pop();
            Console.WriteLine(f);
            //var g = stack.Pop(); //Вызывает ошибку
            //Console.WriteLine(g);
        }
    }
}
