using LRR1;
using NUnit.Framework;
using System;

namespace lr1tests
{
    [TestFixture]
    public class StackTests
    {
        [Test]
        public void EmptyConstructorTest()
        {
            var stack = new MyStack<int>();
            Assert.AreEqual(0, stack.Count);
            Assert.AreEqual(0, stack.Head);
        }

        [Test]
        public void ArrayConstructorTest()
        {
            int[] array = { 1, 2, 3, 4, 5 };
            var stack = new MyStack<int>(array);
            Assert.AreEqual(array.Length, stack.Count);
            Assert.AreEqual(5, stack.Head);
        }

        [Test]
        public void EmptyStackClearTest()
        {
            var stack = new MyStack<int>();
            stack.Clear();
            Assert.AreEqual(0, stack.Count);
            Assert.AreEqual(0, stack.Head);
        }

        [Test]
        public void PushToEmptyStackTest()
        {
            var stack = new MyStack<int>();
            stack.Push(5);
            Assert.AreEqual(1, stack.Count);
            Assert.AreEqual(5, stack.Head);
        }

        [Test]
        public void PushToNotEmptyStackTest()
        {
            var stack = new MyStack<int>(new[] { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
            stack.Push(9);
            Assert.AreEqual(12, stack.Count);
            Assert.AreEqual(9, stack.Head);
        }
        [Test]
        public void PopFromNotEmptyStackTest()
        {
            var stack = new MyStack<int>(new[] { 6, 5, 4, 3, 2, 1 });
            var actual = stack.Pop();
            Assert.AreEqual(1, actual);
            Assert.AreEqual(5, stack.Count);
        }

        [Test]
        public void ClearNotEmptyStackTest()
        {
            var stack = new MyStack<int>(new[] { 6, 5, 4, 3, 2, 1 });
            stack.Clear();
            Assert.AreEqual(0, stack.Count);
            Assert.AreEqual(0, stack.Head);
        }

        [Test]
        public void PopLastFromNotEmptyStackTest()
        {
            var stack = new MyStack<char>(new[] { 'a' });
            var actual = stack.Pop();
            Assert.AreEqual('a', actual);
            Assert.AreEqual(0, stack.Count);
            Assert.AreEqual('\0', stack.Head);
        }
        [Test]
        public void PopFromEmptyStackTest()
        {
            var stack = new MyStack<double>();
            Assert.Throws<IndexOutOfRangeException>(() => stack.Pop());
        }
    }
}